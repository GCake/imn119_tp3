﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private float m_MoveSpeed = 5;

    private Transform m_Transform;

    private Plane m_Plane;
    private Ray m_MouseRay;

    private Vector3 m_NewPosition;
    private Vector3 m_NewDirection;
    private Vector3 m_Decal;

    private void Awake()
    {
        m_Transform = GetComponent<Transform>();
        m_Plane = new Plane(Vector3.up, m_Transform.position.y - m_Transform.localScale.y/2);
        m_Decal = new Vector3(0.1f, 0.1f, 0.1f);
    }

    private void Start()
    {
        m_NewDirection = Vector3.forward;
    }

    private void Update()
    {
        if (HUDManager.IsInGame())
        {
            if (Input.GetMouseButtonDown(1)) //1 = clic droit
            {
                if (!HUDManager.Skill1IsInUse())
                {
                    m_MouseRay = UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition);
                    float hitDistance;

                    if (m_Plane.Raycast(m_MouseRay, out hitDistance))
                    {
                        m_NewPosition = m_MouseRay.GetPoint(hitDistance);
                        m_NewDirection = new Vector3(m_MouseRay.GetPoint(hitDistance).x - m_Transform.position.x, 0, m_MouseRay.GetPoint(hitDistance).z - m_Transform.position.z);

                    }
                }
            }

            if (Input.GetMouseButtonDown(0)) //0 = clic gauche
            {
                if (HUDManager.Skill1IsInUse()) //si le skill est utilisé
                {
                    m_NewPosition = m_Transform.position; //on s'arrete

                    m_MouseRay = UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition);
                    float hitDistance;
                    if (m_Plane.Raycast(m_MouseRay, out hitDistance))
                    {
                        //et on se tourne dans la direction du skill
                        m_NewDirection = new Vector3(m_MouseRay.GetPoint(hitDistance).x - m_Transform.position.x, 0, m_MouseRay.GetPoint(hitDistance).z - m_Transform.position.z);
                    }
                }
            }
        }
    }

    void FixedUpdate()
    {
        if (HUDManager.IsInGame())
        {
            if (!IsOnNewPosition())
            {
                m_Transform.Translate(m_NewDirection.normalized * m_MoveSpeed * Time.deltaTime, Space.World);
            }

            if (!IsOnNewRotation())
            {
                Quaternion from = m_Transform.rotation;
                Quaternion to = Quaternion.LookRotation(m_NewDirection.normalized);
                m_Transform.rotation = Quaternion.RotateTowards(from, to, 30);
            }
        }
    }

    private bool IsOnNewPosition()
    {
        if (m_Transform.position.x < (m_NewPosition + m_Decal).x &&
           m_Transform.position.x > (m_NewPosition - m_Decal).x &&
           m_Transform.position.z < (m_NewPosition + m_Decal).z &&
           m_Transform.position.z > (m_NewPosition - m_Decal).z)
            return true;
        return false;
    }

    private bool IsOnNewRotation()
    {
        Quaternion newRotation = Quaternion.LookRotation(m_NewDirection.normalized);
        if (Quaternion.Angle(m_Transform.rotation, newRotation) < 0.1f)
            return true;
        return false;
    }
}
