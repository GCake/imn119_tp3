﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
    [Header("Panels")]
    [SerializeField]
    private GameObject m_TutoPanel;

    [Header("FireBurst")]
    [SerializeField]
    private GameObject m_BurstFireCursor;
    private static bool m_Skill1_IsInUse;

    private GameObject m_Player;
    private static bool m_InGame;

    private void Awake()
    {
        m_Skill1_IsInUse = false;
        m_InGame = false;
    }

    private void Start()
    {
        m_Player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        if (HUDManager.IsInGame())
        {
            if (Input.GetKeyDown(KeyCode.Alpha1)) //Keybind
            {
                Btn_Skill1_Clicked();
            }
        }
    }

    public void Btn_Skill1_Clicked()
    {
        Debug.Log("btnskill1 has been clicked !");
        if (!m_Skill1_IsInUse)
        {
            m_Skill1_IsInUse = true;
            if (m_Player != null)
            {
                if (m_BurstFireCursor != null)
                {
                    GameObject obj = Instantiate(m_BurstFireCursor);
                }
            }
        }
        else
        {
            Debug.Log("The skill is already in use !");
        }
    }

    public static void CancelSkill1InUse()
    {
        m_Skill1_IsInUse = false;
    }

    public static bool Skill1IsInUse()
    {
        return m_Skill1_IsInUse;
    }

    public static bool IsInGame()
    {
        return m_InGame;
    }

    public void Btn_Compris_Clicked()
    {
        if(m_TutoPanel != null)
        {
            m_TutoPanel.SetActive(false);
            m_InGame = true;
        }
    }
}
