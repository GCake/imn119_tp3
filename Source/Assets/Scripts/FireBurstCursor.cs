﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBurstCursor : MonoBehaviour
{
    [SerializeField]
    private GameObject m_FireBurst;

    private Plane m_Plane;
    private Ray m_MouseRay;

    private Transform m_Transform;

    // Start is called before the first frame update
    void Start()
    {
        m_Transform = GetComponent<Transform>();
        m_Plane = new Plane(Vector3.up, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (HUDManager.IsInGame())
        {
            m_MouseRay = UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition);
            float hitDistance;

            if (m_Plane.Raycast(m_MouseRay, out hitDistance))
            {
                m_Transform.position = m_MouseRay.GetPoint(hitDistance);
            }

            //Lancer le sort a l'endroit si on clic gauche
            if (Input.GetMouseButtonDown(0)) //clic gauche
            {
                if (m_FireBurst != null)
                {
                    GameObject obj = Instantiate(m_FireBurst);
                    obj.transform.position = m_Transform.position;
                    HUDManager.CancelSkill1InUse();
                }
                Destroy(this.gameObject);
            }

            //Annuler le sort si on clic droit
            if (Input.GetMouseButtonDown(1)) //clic droit
            {
                HUDManager.CancelSkill1InUse();
                Destroy(this.gameObject);
            }
        }
    }
}
