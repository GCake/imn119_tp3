﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    private GameObject m_Player;
    private Vector3 m_CurrVelocity;
    private float m_SmoothTime = 0.4f;
    
    void Start()
    {
        m_Player = GameObject.FindGameObjectWithTag("Player");
    }

    void FixedUpdate()
    {
        if (HUDManager.IsInGame())
        {
            if (m_Player != null)
            {
                float posX = Mathf.SmoothDamp(transform.position.x, m_Player.transform.position.x, ref m_CurrVelocity.x, m_SmoothTime);
                float posZ = Mathf.SmoothDamp(transform.position.z, m_Player.transform.position.z - 5f, ref m_CurrVelocity.z, m_SmoothTime);

                transform.position = new Vector3(posX, transform.position.y, posZ);
            }
        }
    }
}
